@echo off
cls

set APP="WebApplication1"
set DIST_PATH="C:\Distribution\Test"
set FULL_PATH="C:\Distribution\Test\WebApplication1.exe"

IF EXIST %DIST_PATH% (
    sc stop %APP%
) ELSE (
    mkdir %DIST_PATH%
    sc create %APP% binPath=%FULL_PATH%
)

dotnet publish -c release -o %DIST_PATH%
sc start %APP%